# Zola Dock Theme

A theme for use with [Zola](https://www.getzola.org/), which provides a dock-like sidebar.


## Variables

Under the `[extra]` table in your `config.toml`, the following variables are supported for use with the theme.

The `sidebar_pages` variable controls the content in the upper left portion of the sidebar, whereas the `sidebar_social` variable controls the bottom left portion of the sidebar.

The `index_css` variable defines additional CSS files to include in the header for the index page, and all pages built from the index page.

```toml
sidebar_pages = [
    { id = "Home", url = "/", img = "home.svg" }
]

sidebar_social = [
    { id ="GitHub", url = "https://github.com/mmstick", img = "social/github.svg" },
    { id ="GitLab", url = "https://gitlab.com/mmstick", img = "social/gitlab.svg" },
    { id ="Twitter", url = "https://twitter.com/mmstick", img = "social/twitter.svg"},
    { id ="Mastodon", url = "https://fosstodon.org/@mmstick", img = "social/mastodon.svg" },
    { id ="Keybase", url = "https://keybase.io/mmstick", img = "social/keybase.svg" },
    { id ="Email", url = "mailto:mmstick@pm.me", img = "social/email.svg" }
]
```
